﻿var express = require('express');
var sys = require('sys');
var hash = require('node_hash');
var TYPES = require('tedious').TYPES;
var Request = require('tedious').Request;
var Connection = require('tedious').Connection;

//This is sql connection info. This should probably be in its on module.
var config = {
	userName: 'pnkadmin',
	password: 'SuperStrongPassword1',
	server: 'pnk.database.windows.net', 
	options: {
		database:'PNKDB',
		useColumnNames:true,
		encrypt: true 
	}
}

//constructor
var User = function () {
	this.data = {};
	this.data.id = 0
	this.data.firstName = '';
	this.data.lastName = '';
	this.data.email = '';
	this.data.description = '';
	this.data.password = '';
}

User.prototype.data = {}
//sql statements. There are ORM's out there to use, but this is the fastest method and doesnt require choosing a framework to work with.
User.prototype.SelectStatement = 'SELECT TOP 1 * FROM dbo.USERS WHERE Email = @Email AND Password = @Password';
User.prototype.InsertStatement = 'INSERT INTO dbo.USERS(FirstName,LastName,Email,Password,Description) VALUES(@FirstName,@LastName,@Email,@Password,@Description)';
User.prototype.UpdateStatement = 'UPDATE dbo.USERS SET FirstName = @FirstName, LastName=@LastName,Email=@Email,Description=@Description WHERE Id=@Id';

//getters and setters. We could probably get rid of these, but they help with intellisense.
User.prototype.setFirstName = function (firstName) {
	this.data.firstName = firstName;
}

User.prototype.getFirstName = function () {
	return this.data.firstName;
}

User.prototype.setLastName = function (lastName) {
	this.data.lastName = lastName;
}

User.prototype.getLastName = function () {
	return this.data.lastName;
}

User.prototype.setEmail = function (email) {
	this.data.email = email;	
}

User.prototype.getEmail = function () {
	return this.data.email;	
}

User.prototype.setDescription = function (description) {
	this.data.description = description;
}

User.prototype.getDescription = function () {
	return this.data.description;
}

User.prototype.setAndHashPassword = function (password) { //when setting the password always use this function since it hashes the password. 
	var hashedPassword = hash.md5(password); //uses md5 hashing, The hash module has sha1 and a couple other hashing methods as well. There are more complex solutions to include salt as well.
	this.data.password = hashedPassword;	
}

User.prototype.getHashedPassword = function () {
	return this.data.password;	
}

//db logic to insert a user.
User.prototype.Insert = function (callback) {
	var userData = this.data;
	var connection = new Connection(config);
	connection.on('connect', function (err) { //connect to the db
		if (err) { //if error log, and return
			console.log(err);
			callback(err);
		}
		
		var Req = new Request(User.prototype.InsertStatement, function (err) { //setup request
			if (err) { //if error log
				console.log(JSON.stringify(err));
			}
			callback(err); //on ether success or failure return
		});
		
		//parameterize the variables so we dont get sql injected.
		Req.addParameter("FirstName", TYPES.VarChar, userData.firstName); 
		Req.addParameter("LastName", TYPES.VarChar, userData.lastName);
		Req.addParameter("Email", TYPES.VarChar, userData.email);
		Req.addParameter("Password", TYPES.VarChar, userData.password);
		Req.addParameter("Description", TYPES.VarChar, userData.description);
		connection.execSql(Req);
	});
}

User.prototype.Update = function (callback) {
	if (this.data.id <= 0) { //if the id is set to 0, then this must be a new user, and should be inserted not updated.
		callback('Must be an existing object in the database');
		return;
	}
	var userData = this.data;
	var connection = new Connection(config);
	connection.on('connect', function (err) { //connect to the db
		if (err) { //if error log and return
			console.log(err);
			callback(err);
		}
		
		var Req = new Request(User.prototype.UpdateStatement, function (err) { //setup the request
			if (err) {// if error log
				console.log(JSON.stringify(err));
			}
			callback(err); //return if error or not.
		});
		
		//setup paramameters so we dont get sql injected.
		Req.addParameter("FirstName", TYPES.VarChar, userData.firstName); 
		Req.addParameter("LastName", TYPES.VarChar, userData.lastName);
		Req.addParameter("Email", TYPES.VarChar, userData.email);
		Req.addParameter("Description", TYPES.VarChar, userData.description);
		Req.addParameter("Id", TYPES.Int, userData.id);
		connection.execSql(Req);
	});
}

User.prototype.Find = function (callback) {
	var userData = this.data;
	var connection = new Connection(config);
	connection.on('connect', function (err) { //connect to the db
		if (err) { //if error log and return
			console.log(err);
			callback(err);
		}
		
		var Req = new Request(User.prototype.SelectStatement, function (err,rowcount) { //setup request
			if (err || rowcount <= 0) { //if there was an error, or we didnt find any user matching the email/password combo then callback with error
				if (!err) { //if theres no error, we must not have found the user.
					err = 'No user found';	
				}
				console.log(JSON.stringify(err));
				callback(err);
			}
			
		});
		Req.addParameter("Email", TYPES.VarChar, userData.email);
		Req.addParameter("Password", TYPES.VarChar, userData.password);
		Req.on('row', function (columns) { //setup row event
			userData.id = columns.Id.value //if we get a row setup the userdata to equal that of the user data in the row.
			userData.firstName = columns.FirstName.value;
			userData.lastName = columns.LastName.value;
			userData.description = columns.Description.value;
			callback(null); //there should only ever be one row, so return now.
		});

		connection.execSql(Req);
	});
}

module.exports = User;